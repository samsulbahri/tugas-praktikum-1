package com.samsulbahri_10181072.praktikum1.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.samsulbahri_10181072.praktikum1.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}